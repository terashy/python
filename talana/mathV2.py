"""
comment section

"""
number1 = 2
number2 = 2

substract = (number1+number2)
minus = (number1-number2)
addition = (number1*number2)
power = (number1**number2)  # power
power2 = (number1**5)   # power ("2*2*2*2*2")
division = (number1/number2)
modular = (10 % 3)  # Modular


substract = f'The Result is {substract}!'
minus = f'The Result is {minus}!'
addition = f'The Result is {addition}!'
power = f'The Result is {power}!'
power2 = f'The Result is {power2}!'
division = f'The Result is {division}!'
modular = f'The Result is {modular}!'

# oneLiner
# print('',  substract, '\n', minus, "\n", addition, '\n',
#   power, '\n', power2, '\n', division, '\n', modular)

print(substract)
print(minus)

# result = 1+3
# result *= 2

# print(2-2)
# print(2*2)
# print(2**2)  # power
# print(2**5)  # power ("2*2*2*2*2")
# print(2/2)
# print(10 % 3)  # Modular


# print("2 ^ 8=")
# print(2 ^ 8)  # ! ???
